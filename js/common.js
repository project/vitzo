$(document).ready(function(){
	//styleSearchForm();
	//animateTopnavMenus();
	applyInputHints();
	roundedCorners();
});

function animateTopnavMenus() {
	$(".nav>li").hover(
		function() {$(".subnav",this).hide().slideDown('fast');}, 
		function() {$(".subnav",this).hide()} 
	);
}

function styleSearchForm() {
	img = '<img src="/lib/img/ico/apps/24x24/shadow/view.png" alt="'+Drupal.t('Search')+'" />';
	linkedImg = '<a href="#" onclick="$(\'#search-theme-form\').submit(); return false;">'+img+'</a>';
	$('#search-container input[@type=submit]').replaceWith(linkedImg);
}

function applyInputHints() {
	$('#search-container input[type=text]').hint();
}

function roundedCorners() {
	//$('#search-container').corner('10px');
}
